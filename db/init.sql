create table if not exists service
(
    id   integer not null
        primary key,
    name varchar(255)
);

create table if not exists endpoint
(
    id         integer     not null
        primary key,
    method     varchar(16) not null,
    name       varchar(255),
    service_id integer
        constraint endpoint_has_service
            references service,
    constraint endpoint_method_uniq
        unique (name, service_id, method)
);

create table if not exists probe
(
    id          bigint not null
        primary key,
    code        integer,
    probe_time  bigint,
    success     integer,
    time        integer,
    endpoint_id integer
        constraint probe_has_endpoint
            references endpoint
);

create table if not exists probe_1d_sum
(
    id               bigserial not null primary key,
    sum_time         integer not null,
    success_count    integer not null,
    count            integer not null,
    probe_trunc_time integer not null,
    endpoint_id      integer not null
        constraint probe_1d_sum_endpoint_fk
            references endpoint,
        constraint probe_1d_uniq
            unique (endpoint_id, probe_trunc_time)
);

create table if not exists probe_6h_sum
(
    id               bigserial not null primary key,
    sum_time         integer not null,
    success_count    integer not null,
    count            integer not null,
    probe_trunc_time integer not null,
    endpoint_id      integer not null
        constraint probe_1h_sum_endpoint_fk
            references endpoint,
        constraint probe_6h_uniq
            unique (endpoint_id, probe_trunc_time)
);

create or replace function maint_probe_1d_sum() returns trigger
    language plpgsql
as
$$
begin
    insert into probe_1d_sum (sum_time, success_count, count, probe_trunc_time, endpoint_id)
    values (new.time, new.success, 1, extract(epoch from date_trunc('day', to_timestamp(new.probe_time))), new.endpoint_id)
    on conflict on constraint probe_1d_uniq do update
    set
        sum_time = probe_1d_sum.sum_time + new.time,
        success_count = probe_1d_sum.success_count + new.success,
        count = probe_1d_sum.count + 1;
    return null;
end;
$$;

create trigger maint_probe_1d_sum
    after insert
    on probe
    for each row
execute function maint_probe_1d_sum();

create or replace function maint_probe_6h_sum() returns trigger
    language plpgsql
as
$$
begin
    insert into probe_6h_sum (sum_time, success_count, count, probe_trunc_time, endpoint_id)
    values (new.time, new.success, 1, extract(epoch from date_trunc('hour', to_timestamp(new.probe_time / 6))) * 6, new.endpoint_id)
    on conflict on constraint probe_6h_uniq do update
        set
            sum_time = probe_6h_sum.sum_time + excluded.sum_time,
            success_count = probe_6h_sum.success_count + excluded.success_count,
            count = probe_6h_sum.count + 1;
    return null;
end;
$$;

create trigger maint_probe_6h_sum
    after insert
    on probe
    for each row
execute function maint_probe_6h_sum();
import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Modifier;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.nodeTypes.modifiers.NodeWithStaticModifier;
import com.github.javaparser.ast.visitor.GenericVisitor;
import com.github.javaparser.ast.visitor.ModifierVisitor;
import com.github.javaparser.ast.visitor.Visitable;
import com.github.javaparser.ast.visitor.VoidVisitor;
import com.github.javaparser.utils.SourceRoot;

import io.swagger.parser.OpenAPIParser;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.PathItem;
import io.swagger.v3.oas.models.Paths;
import io.swagger.v3.parser.core.models.SwaggerParseResult;

@Mojo(name = "endpoint-generator", defaultPhase = LifecyclePhase.GENERATE_SOURCES)
public class Main extends AbstractMojo {

    @Parameter(property = "schemaUrl", required = true, readonly = true)
    public String schemaUrl;

    @Parameter(property = "endpointPathField", defaultValue = "endpointName", required = true, readonly = true)
    public String endpointPathField;

    @Parameter(property = "methodField", defaultValue = "method", required = true, readonly = true)
    public String methodField;

    @Parameter(property = "sourceDir", required = true, readonly = true)
    public String sourceDir;

    @Parameter(property = "endpointsPackage", required = true, readonly = true)
    public String endpointsPackage;

    @Parameter(defaultValue = "${project}", required = true, readonly = true)
    public MavenProject project;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        SwaggerParseResult result = new OpenAPIParser().readLocation(schemaUrl, null, null);
        OpenAPI openAPI = result.getOpenAPI();
        if (result.getMessages() != null) {
            result.getMessages().forEach(System.err::println);
        }
        Map<Endpoint, String> schemaEndpoints = new HashMap<>();
        Paths paths = openAPI.getPaths();
        for (Map.Entry<String, PathItem> path : paths.entrySet()) {
            for (Map.Entry<PathItem.HttpMethod, Operation> entry : path.getValue().readOperationsMap().entrySet()) {
                StringBuilder operationName = new StringBuilder(entry.getValue().getOperationId());
                operationName.setCharAt(0, Character.toUpperCase(operationName.charAt(0)));
                schemaEndpoints.put(new Endpoint(path.getKey(), entry.getKey().name()), operationName.toString());
            }
        }
        Path path = project.getBasedir().toPath().resolve(sourceDir);
        SourceRoot sr = new SourceRoot(path);
        SourceRoot readSr = new SourceRoot(path);
        System.out.println("Implemented endpoint classes.");
        Set<Endpoint> implementedEndpoints = new HashSet<>();
        try {
            readSr.tryToParse(endpointsPackage);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        for (CompilationUnit cu : readSr.getCompilationUnits()) {
            Endpoint endpoint = new Endpoint();
            cu.findAll(FieldDeclaration.class).stream()
                    .filter(NodeWithStaticModifier::isStatic)
                    .forEach(f -> {
                        if (f.getVariables().size() == 1) {
                            if (f.getVariable(0).getNameAsString().equals(endpointPathField)) {
                                Optional<Expression> opt = f.getVariable(0).getInitializer();
                                if (opt.isPresent()) {
                                    Expression exp = opt.get();
                                    if (exp.isStringLiteralExpr()) {
                                        endpoint.setEndpointName(exp.asStringLiteralExpr().asString());
                                    }
                                }
                            }
                            if (f.getVariable(0).getNameAsString().equals(methodField)) {
                                Optional<Expression> opt = f.getVariable(0).getInitializer();
                                if (opt.isPresent()) {
                                    Expression exp = opt.get();
                                    if (exp.isStringLiteralExpr()) {
                                        endpoint.setMethod(exp.asStringLiteralExpr().asString());
                                    }
                                }
                            }
                        }
                    });
            if (endpoint.filled()) {
                implementedEndpoints.add(endpoint);
                System.out.printf("Method: %-8s| Path: %-35s|%n",
                        endpoint.getMethod(), endpoint.getEndpointName());
            }
        }
        System.out.println();
        System.out.println("Generating endpoint classes.");
        int generatedCount = 0;
        for (Map.Entry<Endpoint, String> entry : schemaEndpoints.entrySet()) {
            if (!implementedEndpoints.contains(entry.getKey())) {
                generatedCount++;
                String code = "public class " + entry.getValue() +
                        " implements TamTamQuery" +
                        " {" +
                        "private static final String " + endpointPathField +
                        "=\"" + entry.getKey().getEndpointName() + "\";" +
                        "private static final String " + methodField +
                        "=\"" + entry.getKey().getMethod() + "\";" +
                        "}";
                CompilationUnit cu = StaticJavaParser.parse(code);
                cu.setPackageDeclaration(endpointsPackage);
                sr.add(endpointsPackage, entry.getValue() + ".java", cu);
                System.out.printf("Method: %-8s| Path: %-35s| Class name: %-20s|%n",
                        entry.getKey().getMethod(),
                        entry.getKey().getEndpointName(),
                        entry.getValue());
            }
        }
        System.out.println("Total generated classes: " + generatedCount);
        sr.saveAll();
    }
}


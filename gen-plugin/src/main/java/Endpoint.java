import java.util.Objects;

public class Endpoint {
    private String endpointName;
    private String method;

    public Endpoint(){
    }
    public Endpoint(String endpointName, String method) {
        this.endpointName = endpointName;
        this.method = method;
    }

    public String getEndpointName() {
        return endpointName;
    }

    public String getMethod() {
        return method;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Endpoint endpoint = (Endpoint) o;
        return endpointName.equals(endpoint.endpointName) && method.equals(endpoint.method);
    }

    @Override
    public int hashCode() {
        return Objects.hash(endpointName, method);
    }

    @Override
    public String toString() {
        return "Endpoint{" +
                "endpointName='" + endpointName + '\'' +
                ", method='" + method + '\'' +
                '}';
    }

    public void setEndpointName(String endpointName) {
        this.endpointName = endpointName;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public boolean filled() {
        return endpointName != null && method != null;
    }
}

package com.poly.botapistatback;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.poly.botapistatback.dto.EndpointDto;
import com.poly.botapistatback.entity.ProbeDaySum;
import com.poly.botapistatback.entity.custom.EndpointAggregation;
import com.poly.botapistatback.repository.ProbeDaySumRepository;
import com.poly.botapistatback.repository.ServiceRepository;
import com.poly.botapistatback.service.EndpointService;
import com.poly.botapistatback.service.ProbeService;
import com.poly.botapistatback.service.ServiceService;

@SpringBootTest
class BotapiStatBackApplicationTests {

}

package com.poly.botapistatback.service;

import java.util.List;
import java.util.Set;

import com.poly.botapistatback.dto.ProbeAggDto;

public interface ProbeSumService {
    List<ProbeAggDto> getByEndpointFrom(Integer endpointId, Long from, Integer frequency);
}

package com.poly.botapistatback.service;

import java.util.List;
import java.util.Set;


import com.poly.botapistatback.dto.EndpointDto;
import com.poly.botapistatback.dto.ProbeDto;
import com.poly.botapistatback.dto.ServiceDto;

public interface EndpointService {
    EndpointDto findById(Integer id);
    List<EndpointDto> getAll();

    List<EndpointDto> getByService(ServiceDto service);
    List<ProbeDto> getProbe(EndpointDto endpoint, Integer offset, Integer length);

    EndpointDto getByName(String name);

    Integer insertNewEndpoint(String name, String method, Integer service_id);

    Integer getEndpointId(String name, String method, Integer serviceId);

    List<ProbeDto> getProbesFrom(EndpointDto endpoint, Long from);
}

package com.poly.botapistatback.service.impl;

import java.time.Instant;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.poly.botapistatback.entity.Endpoint;
import com.poly.botapistatback.entity.Probe;
import com.poly.botapistatback.repository.EndpointRepository;
import com.poly.botapistatback.repository.ProbeRepository;
import com.poly.botapistatback.repository.ServiceRepository;
import com.poly.botapistatback.service.ProbeService;

@Service
public class ProbeServiceImpl implements ProbeService {
    @Autowired
    private ProbeRepository probeRepository;

    @Autowired
    EndpointRepository endpointRepository;

    @Autowired
    ServiceRepository serviceRepository;

    @Override
    public Boolean insertNewProbe(String endpointName,
                                  String method,
                                  Integer serviceId,
                                  Integer time,
                                  Boolean success,
                                  Integer code,
                                  Long reqTime) {
        Probe probe = new Probe();
        probe.setSuccess(success);
        probe.setTime(time);
        probe.setCode(code);
        com.poly.botapistatback.entity.Service service = serviceRepository.getById(serviceId);
        Endpoint endpoint = endpointRepository.getByNameAndMethodAndService(endpointName, method, service)
                .orElseThrow();
        probe.setEndpoint(endpoint);
        if (reqTime != null) {
            probe.setProbeTime(reqTime);
        }
        try {
            probeRepository.save(probe);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}

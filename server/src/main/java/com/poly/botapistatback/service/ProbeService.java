package com.poly.botapistatback.service;

import com.poly.botapistatback.entity.Endpoint;

public interface ProbeService {
    Boolean insertNewProbe(String endpointName, String method,
                           Integer serviceId, Integer time, Boolean success, Integer code, Long reqTime);

}

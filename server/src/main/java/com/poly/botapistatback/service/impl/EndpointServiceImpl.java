package com.poly.botapistatback.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.poly.botapistatback.dto.EndpointDto;
import com.poly.botapistatback.dto.ProbeDto;
import com.poly.botapistatback.dto.ServiceDto;
import com.poly.botapistatback.dto.converter.EndpointConv;
import com.poly.botapistatback.dto.converter.ProbeConv;
import com.poly.botapistatback.entity.Endpoint;
import com.poly.botapistatback.entity.Probe;
import com.poly.botapistatback.payload.response.Event;
import com.poly.botapistatback.repository.EndpointRepository;
import com.poly.botapistatback.repository.ProbeRepository;
import com.poly.botapistatback.repository.ServiceRepository;
import com.poly.botapistatback.service.EndpointService;

@Service
public class EndpointServiceImpl implements EndpointService {
    @Autowired
    private EndpointRepository repository;

    @Autowired
    private ProbeRepository probeRepository;

    @Autowired
    private ServiceRepository serviceRepository;

    @Override
    public EndpointDto findById(Integer id) {
        Endpoint result = repository.findById(id).orElseThrow();
        return EndpointConv.toDto(result);
    }

    @Override
    public List<EndpointDto> getAll() {
        List<Endpoint> result = repository.getAll();
        return result.stream()
                .map(EndpointConv::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<EndpointDto> getByService(ServiceDto service) {
        return repository.getByServiceId(service.getId())
                .stream()
                .map(EndpointConv::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ProbeDto> getProbe(EndpointDto endpoint, Integer offset, Integer length) {
        Pageable pageable = PageRequest.of(offset / length, length);
        return probeRepository.getProbesByEndpoint(endpoint.getId(), pageable)
                .stream()
                .map(ProbeConv::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ProbeDto> getProbesFrom(EndpointDto endpoint, Long from) {
        return probeRepository.getProbesByEndpointFrom(endpoint.getId(), from)
                .stream()
                .map(ProbeConv::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public EndpointDto getByName(String name) {
        return EndpointConv.toDto(repository.getByName(name).orElseThrow());
    }

    @Override
    public Integer insertNewEndpoint(String name, String method, Integer service_id) {
        com.poly.botapistatback.entity.Service service = serviceRepository.getServiceById(service_id)
                .orElseThrow();
        Endpoint endpoint = new Endpoint();
        endpoint.setService(service);
        endpoint.setMethod(method);
        endpoint.setName(name);
        return repository.save(endpoint).getId();
    }

    @Override
    public Integer getEndpointId(String name, String method, Integer serviceId) {
        Optional<Endpoint> endpoint = repository
                .getByNameAndMethodAndService(name, method, serviceRepository.getById(serviceId));
        if (endpoint.isEmpty()) {
            return null;
        } else {
            return endpoint.get().getId();
        }
    }
}

package com.poly.botapistatback.service.impl;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import com.poly.botapistatback.dto.ServiceDto;
import com.poly.botapistatback.dto.converter.ServiceConv;
import com.poly.botapistatback.entity.Service;
import com.poly.botapistatback.entity.custom.EndpointAggregation;
import com.poly.botapistatback.entity.custom.ProbeSumAggregation;
import com.poly.botapistatback.repository.ServiceRepository;
import com.poly.botapistatback.service.ServiceService;

@org.springframework.stereotype.Service
public class ServiceServiceImpl implements ServiceService {
    @Autowired
    private ServiceRepository serviceRepository;

    @Override
    public List<EndpointAggregation> getEndpointAggregation(Integer service_id, Long from) {
        return serviceRepository.getEndpointAggregation(service_id, from);
    }

    @Override
    public List<EndpointAggregation> getAllEndpointAggregation(Long from) {
        return serviceRepository.getAllEndpointAggregation(from);
    }

    @Override
    public List<ProbeSumAggregation> getProbeSumAggregationByService(Integer serviceId, Long from) {
        return serviceRepository.getSumEndpointAggregation(serviceId, from);
    }

    @Override
    public List<ProbeSumAggregation> getAllProbeSumAggregation(Long from) {
        return serviceRepository.getAllSumEndpointAggregation(from);
    }

    @Override
    public ServiceDto getServiceByName(String name) {
        return ServiceConv.toDto(serviceRepository.getServiceByName(name).orElseThrow());
    }

    @Override
    public Integer insertNewService(String name) {
        Service entity = new Service();
        entity.setName(name);
        return serviceRepository.save(entity).getId();
    }
}

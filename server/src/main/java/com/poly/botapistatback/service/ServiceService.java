package com.poly.botapistatback.service;

import java.util.List;
import java.util.Set;

import com.poly.botapistatback.dto.ServiceDto;
import com.poly.botapistatback.entity.custom.EndpointAggregation;
import com.poly.botapistatback.entity.custom.ProbeSumAggregation;

public interface ServiceService {
    List<EndpointAggregation> getEndpointAggregation(Integer service_id, Long from);

    List<EndpointAggregation> getAllEndpointAggregation(Long from);

    List<ProbeSumAggregation> getProbeSumAggregationByService(Integer serviceId, Long from);

    List<ProbeSumAggregation> getAllProbeSumAggregation(Long from);

    ServiceDto getServiceByName(String name);

    Integer insertNewService(String name);
}

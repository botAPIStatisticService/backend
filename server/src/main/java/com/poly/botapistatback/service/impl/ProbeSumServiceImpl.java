package com.poly.botapistatback.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.poly.botapistatback.dto.ProbeAggDto;
import com.poly.botapistatback.repository.ProbeDaySumRepository;
import com.poly.botapistatback.repository.Probe6HourSumRepository;
import com.poly.botapistatback.service.ProbeSumService;


@Service
public class ProbeSumServiceImpl implements ProbeSumService {

    @Autowired
    ProbeDaySumRepository dayRepository;

    @Autowired
    Probe6HourSumRepository hourRepository;

    @Override
    public List<ProbeAggDto> getByEndpointFrom(Integer endpointId, Long from, Integer frequency) {
        if (frequency == 1) {
            return dayRepository.getByEndpointFrom(endpointId, from).stream()
                    .map((e) -> new ProbeAggDto(e.getSumTime(),
                            e.getSuccessCount(),
                            e.getCount(),
                            e.getProbeTruncTime()))
                    .collect(Collectors.toList());
        } else {
            return hourRepository.getByEndpointFrom(endpointId, from).stream()
                    .map((e) -> new ProbeAggDto(e.getSumTime(),
                            e.getSuccessCount(),
                            e.getCount(),
                            e.getProbeTruncTime()))
                    .collect(Collectors.toList());
        }
    }
}

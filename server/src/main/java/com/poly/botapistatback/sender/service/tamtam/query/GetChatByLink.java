package com.poly.botapistatback.sender.service.tamtam.query;

import java.util.concurrent.ExecutionException;

import org.springframework.stereotype.Component;

import com.poly.botapistatback.sender.service.tamtam.AbstractTamTamQuery;
import com.poly.botapistatback.sender.service.tamtam.TamTamIds;

import chat.tamtam.botapi.TamTamBotAPI;
import chat.tamtam.botapi.exceptions.ClientException;
import chat.tamtam.botapi.model.Chat;

@Component
public class GetChatByLink extends AbstractTamTamQuery {

    private static final String endpointName = "/chats/{chatLink}";

    private static final String method = "GET";

    final private TamTamIds tamTamIds;

    public GetChatByLink(TamTamIds tamTamIds) {
        super(endpointName, method);
        this.tamTamIds = tamTamIds;
    }

    @Override
    public void initialize(TamTamBotAPI botAPI) {
        String chatLink = tamTamIds.getChatLink();
        try {
            super.setQuery(botAPI.getChatByLink(chatLink));
        } catch (ClientException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void execute() {
        super.sendQuery();
    }

    public Chat getData() {
        try {
            return (Chat) super.getFuture().get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }
}

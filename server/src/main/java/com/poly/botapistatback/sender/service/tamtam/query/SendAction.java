package com.poly.botapistatback.sender.service.tamtam.query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.poly.botapistatback.sender.service.tamtam.AbstractTamTamQuery;
import com.poly.botapistatback.sender.service.tamtam.TamTamIds;

import chat.tamtam.botapi.TamTamBotAPI;
import chat.tamtam.botapi.exceptions.ClientException;
import chat.tamtam.botapi.model.ActionRequestBody;
import chat.tamtam.botapi.model.SenderAction;

@Component
public class SendAction extends AbstractTamTamQuery {

    private static final String endpointName = "/chats/{chatId}/actions";

    private static final String method = "POST";

    @Autowired
    TamTamIds tamTamIds;

    public SendAction() {
        super(endpointName, method);
    }
    @Override
    public void initialize(TamTamBotAPI botAPI) {
        Long chatId = tamTamIds.getChatWithNonRemovableMsg();
        ActionRequestBody actionRequestBody = new ActionRequestBody(SenderAction.TYPING_ON);
        try {
            super.setQuery(botAPI.sendAction(actionRequestBody, chatId));
        } catch (ClientException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void execute() {
        super.sendQuery();
    }
}

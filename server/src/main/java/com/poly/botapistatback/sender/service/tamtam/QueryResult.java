package com.poly.botapistatback.sender.service.tamtam;

public class QueryResult {
    private Boolean isSuccess;
    private String endpointName;

    private String method;

    public QueryResult(Boolean isSuccess, String endpointName, String method) {
        this.isSuccess = isSuccess;
        this.endpointName = endpointName;
        this.method = method;
    }

    public Boolean getSuccess() {
        return isSuccess;
    }

    public String getEndpointName() {
        return endpointName;
    }

    public String getMethod() {
        return method;
    }

    @Override
    public String toString() {
        return "QueryResult{" +
                "isSuccess=" + isSuccess +
                ", endpointName='" + endpointName + '\'' +
                ", method='" + method + '\'' +
                '}';
    }
}

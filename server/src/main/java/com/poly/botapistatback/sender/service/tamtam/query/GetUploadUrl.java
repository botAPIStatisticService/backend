package com.poly.botapistatback.sender.service.tamtam.query;

import java.util.concurrent.Future;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.poly.botapistatback.sender.service.tamtam.EndpointInfo;
import com.poly.botapistatback.sender.service.tamtam.EndpointNameConverter;
import com.poly.botapistatback.sender.service.tamtam.QueryResult;
import chat.tamtam.botapi.TamTamBotAPI;
import chat.tamtam.botapi.exceptions.ClientException;
import chat.tamtam.botapi.model.UploadEndpoint;
import chat.tamtam.botapi.model.UploadType;
import chat.tamtam.botapi.queries.GetUploadUrlQuery;

@Component
public class GetUploadUrl {

    private static final String endpointName = "/uploads";

    private static final String method = "POST";

    private GetUploadUrlQuery getUploadUrlQuery;

    private UploadEndpoint endpoint;

    private Future<UploadEndpoint> response;

    public void initialize(TamTamBotAPI botAPI) {
        try {
            getUploadUrlQuery = botAPI.getUploadUrl(UploadType.IMAGE);
        } catch (ClientException e) {
            throw new RuntimeException(e);
        }
    }

    public void execute() {
        try {
            response = getUploadUrlQuery.enqueue();
        } catch (ClientException e) {
            throw new RuntimeException(e);
        }
    }

    public QueryResult getResult() {
        try {
            endpoint = response.get();
            return new QueryResult(true, endpointName, method);
        } catch (Exception e) {
            return new QueryResult(false, endpointName, method);
        }
    }

    public EndpointInfo getEndpointInfo() {
        return new EndpointInfo(endpointName, method);
    }

    public String getUploadUrl() {
        return endpoint.getUrl();
    }

    public Future<UploadEndpoint> getFutureResult() {
        return response;
    }
}

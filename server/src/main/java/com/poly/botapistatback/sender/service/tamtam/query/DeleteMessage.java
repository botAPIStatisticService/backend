package com.poly.botapistatback.sender.service.tamtam.query;

import java.util.concurrent.Future;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.poly.botapistatback.sender.service.tamtam.EndpointInfo;
import com.poly.botapistatback.sender.service.tamtam.EndpointNameConverter;
import com.poly.botapistatback.sender.service.tamtam.QueryResult;

import chat.tamtam.botapi.TamTamBotAPI;
import chat.tamtam.botapi.exceptions.ClientException;
import chat.tamtam.botapi.model.SimpleQueryResult;
import chat.tamtam.botapi.queries.DeleteMessageQuery;

@Component
public class DeleteMessage {

    private static final String endpointName = "/messages";

    private static final String method = "DELETE";

    private DeleteMessageQuery deleteMessageQuery;

    private Future<SimpleQueryResult> response;

    public void initialize(TamTamBotAPI botAPI, String messageId) {
        try {
            deleteMessageQuery = botAPI.deleteMessage(messageId);
        } catch (ClientException e) {
            throw new RuntimeException(e);
        }
    }

    public void execute() {
        try {
            response = deleteMessageQuery.enqueue();
        } catch (ClientException e) {
            throw new RuntimeException(e);
        }
    }

    public QueryResult getResult() {
        try {
            response.get();
            return new QueryResult(true, endpointName, method);
        } catch (Exception e) {
            return new QueryResult(false, endpointName, method);
        }
    }

    public EndpointInfo getEndpointInfo() {
        return new EndpointInfo(endpointName, method);
    }

    public Future<SimpleQueryResult> getFutureResult() {
        return response;
    }
}

package com.poly.botapistatback.sender.service.tamtam.query;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Component;

import com.poly.botapistatback.sender.service.tamtam.AbstractTamTamQuery;
import com.poly.botapistatback.sender.service.tamtam.TamTamIds;

import chat.tamtam.botapi.TamTamBotAPI;
import chat.tamtam.botapi.exceptions.ClientException;
import chat.tamtam.botapi.model.ChatPatch;

@Component
public class EditChatInfo extends AbstractTamTamQuery {

    private static final String endpointName = "/chats/{chatId}";

    private static final String method = "PATCH";

    private Long chatId;
    private TamTamBotAPI botAPI;

    final
    TamTamIds tamTamIds;

    public EditChatInfo(TamTamIds tamTamIds) {
        super(endpointName, method);
        this.tamTamIds = tamTamIds;
    }
    @Override
    public void initialize(TamTamBotAPI botAPI) {
        this.botAPI = botAPI;
        chatId = tamTamIds.getChatWithNonRemovableMsg();
        ChatPatch chatPatch = new ChatPatch();
        chatPatch.setTitle("NewTitle");
        try {
            super.setQuery(botAPI.editChat(chatPatch, chatId));
        } catch (ClientException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void execute() {
        ChatPatch chatPatch = new ChatPatch();
        chatPatch.setTitle(RandomStringUtils.random(10, true, true));
        try {
            super.setQuery(botAPI.editChat(chatPatch, chatId));
            super.sendQuery();
        } catch (ClientException e) {
            throw new RuntimeException(e);
        }
    }
}

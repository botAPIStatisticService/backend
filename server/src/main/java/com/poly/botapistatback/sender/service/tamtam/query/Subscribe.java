package com.poly.botapistatback.sender.service.tamtam.query;

import org.springframework.stereotype.Component;

import com.poly.botapistatback.sender.service.tamtam.AbstractTamTamQuery;
import com.poly.botapistatback.sender.service.tamtam.TamTamIds;

import chat.tamtam.botapi.TamTamBotAPI;
import chat.tamtam.botapi.exceptions.ClientException;
import chat.tamtam.botapi.model.SubscriptionRequestBody;

@Component
public class Subscribe extends AbstractTamTamQuery {

    private static final String endpointName = "/subscriptions";

    private static final String method = "POST";

    private final TamTamIds tamTamIds;

    public Subscribe(TamTamIds tamTamIds) {
        super(endpointName, method);
        this.tamTamIds = tamTamIds;
    }
    @Override
    public void initialize(TamTamBotAPI botAPI) {
        String url = tamTamIds.getSubscriptionUrl();
        SubscriptionRequestBody subscriptionRequestBody = new SubscriptionRequestBody(url);
        try {
            super.setQuery(botAPI.subscribe(subscriptionRequestBody));
        } catch (ClientException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void execute() {
        super.sendQuery();
    }
}

package com.poly.botapistatback.sender.service.tamtam.query;

public class AnswerOnCallback {

    private static final String endpointName = "/answers";

    private static final String method = "POST";
}

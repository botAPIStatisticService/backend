package com.poly.botapistatback.sender.service.tamtam;

import org.springframework.stereotype.Component;

import com.poly.botapistatback.sender.service.tamtam.query.UploadImage;

@Component
public class EndpointNameConverter {

    public String convert(String url) {
        String[] points = url.split("/");
        if (points[0].equals("https:") || points.length == 1) {
            return "UploadImage";
        }
        if (points.length <= 2) {
            if (points[1].equals("uploadImage")) {
                return "UploadImage";
            }
            return url;
        }
        if (points[1].equals("chats")) {
            if (points.length == 3) {
                if (points[2].charAt(0) == '-') {
                    return "/chats/{chatId}";
                } else {
                    return "/chats/{chatLink}";
                }
            }
            if (points.length == 4) {
                if (points[3].equals("actions")) {
                    return "/chats/{chatId}/actions";
                }
                if (points[3].equals("pin")) {
                    return "/chats/{chatId}/pin";
                }
                if (points[3].equals("members")) {
                    return "/chats/{chatId}/members";
                }
            }
            if (points.length == 5) {
                if (points[4].equals("me")) {
                    return "/chats/{chatId}/members/me";
                }
                if (points[4].equals("admins")) {
                    return "/chats/{chatId}/members/admins";
                }
            }
        }
        if (points[1].equals("messages")) {
            return "/messages/{messageId}";
        }
        return url;
    }
}

package com.poly.botapistatback.sender.service.tamtam.query;

import org.springframework.stereotype.Component;

import com.poly.botapistatback.sender.service.tamtam.AbstractTamTamQuery;
import com.poly.botapistatback.sender.service.tamtam.TamTamIds;

import chat.tamtam.botapi.TamTamBotAPI;

@Component
public class GetMessages extends AbstractTamTamQuery {

    private static final String endpointName = "/messages";

    private static final String method = "GET";

    final private TamTamIds tamTamIds;

    public GetMessages(TamTamIds tamTamIds) {
        super(endpointName, method);
        this.tamTamIds = tamTamIds;
    }
    @Override
    public void initialize(TamTamBotAPI botAPI) {
        Long chatId = tamTamIds.getChatWithNonRemovableMsg();
        super.setQuery(botAPI.getMessages().chatId(chatId));
    }

    @Override
    public void execute() {
        super.sendQuery();
    }
}

package com.poly.botapistatback.sender.service.tamtam.query;

public class LeaveChat {

    private static final String endpointName = "/chats/{chatId}/members/me";

    private static final String method = "DELETE";
}

package com.poly.botapistatback.sender.service.tamtam.query;

import java.util.concurrent.Future;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Component;
import com.poly.botapistatback.sender.service.tamtam.EndpointInfo;
import com.poly.botapistatback.sender.service.tamtam.QueryResult;

import chat.tamtam.botapi.TamTamBotAPI;
import chat.tamtam.botapi.exceptions.ClientException;
import chat.tamtam.botapi.model.NewMessageBody;
import chat.tamtam.botapi.model.SendMessageResult;
import chat.tamtam.botapi.queries.SendMessageQuery;

@Component
public class SendMessage {

    private static final String endpointName = "/messages";

    private static final String method = "POST";

    private Long chatId;

    private SendMessageQuery sendMessageQuery;

    private Future<SendMessageResult> response;

    private SendMessageResult sendMessageResult;

    private TamTamBotAPI botAPI;

    private int skip = 61;

    private int skipped = 0;

    public void initialize(TamTamBotAPI botAPI, Long chatId) {
        this.chatId = chatId;
        this.botAPI = botAPI;
        sendMessageResult = null;
        response = null;
        NewMessageBody newMessageBody = new NewMessageBody("Hello", null, null);
        try {
            sendMessageQuery = botAPI.sendMessage(newMessageBody).chatId(chatId);
        } catch (ClientException e) {
            throw new RuntimeException(e);
        }
    }

    public void execute() {
        sendMessageResult = null;
        response = null;
        if (skipped == 0) {
            NewMessageBody newMessageBody = new NewMessageBody(RandomStringUtils.random(20, true, true), null, null);
            try {
                sendMessageQuery = botAPI.sendMessage(newMessageBody).chatId(chatId);
                response = sendMessageQuery.enqueue();
            } catch (ClientException e) {
                throw new RuntimeException(e);
            }
        }
        skipped++;
        skipped = skipped % skip;
    }

    public QueryResult getResult() {
        try {
            sendMessageResult = response.get();
            return new QueryResult(true, endpointName, method);
        } catch (Exception e) {
            return new QueryResult(false, endpointName, method);
        }
    }

    public EndpointInfo getEndpointInfo() {
        return new EndpointInfo(endpointName, method);
    }

    public String getMessageId() {
        return sendMessageResult.getMessage().getBody().getMid();
    }

    public Future<SendMessageResult> getFutureResult() {
        return response;
    }

    public void setSkip(int skip) {
        this.skip = Math.max(skip, 1);
    }
}

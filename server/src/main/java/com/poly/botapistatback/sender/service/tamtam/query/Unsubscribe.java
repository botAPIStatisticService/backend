package com.poly.botapistatback.sender.service.tamtam.query;

import org.springframework.stereotype.Component;

import com.poly.botapistatback.sender.service.tamtam.AbstractTamTamQuery;
import com.poly.botapistatback.sender.service.tamtam.TamTamIds;

import chat.tamtam.botapi.TamTamBotAPI;
import chat.tamtam.botapi.exceptions.ClientException;

@Component
public class Unsubscribe extends AbstractTamTamQuery {

    private static final String endpointName = "/subscriptions";

    private static final String method = "DELETE";

    private final TamTamIds tamTamIds;

    public Unsubscribe(TamTamIds tamTamIds) {
        super(endpointName, method);
        this.tamTamIds = tamTamIds;
    }

    @Override
    public void initialize(TamTamBotAPI botAPI) {
        String url = tamTamIds.getSubscriptionUrl();
        try {
            super.setQuery(botAPI.unsubscribe(url));
        } catch (ClientException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void execute() {
        super.sendQuery();
    }
}
package com.poly.botapistatback.sender.service.tamtam.query;

import org.springframework.stereotype.Component;

import com.poly.botapistatback.sender.service.tamtam.AbstractTamTamQuery;
import com.poly.botapistatback.sender.service.tamtam.TamTamIds;

import chat.tamtam.botapi.TamTamBotAPI;
import chat.tamtam.botapi.exceptions.ClientException;

@Component
public class GetChatAdmins extends AbstractTamTamQuery {

    private static final String endpointName = "/chats/{chatId}/members/admins";

    private static final String method = "GET";

    final private TamTamIds tamTamIds;

    public GetChatAdmins(TamTamIds tamTamIds) {
        super(endpointName, method);
        this.tamTamIds = tamTamIds;
    }
    @Override
    public void initialize(TamTamBotAPI botAPI) {
        Long chatId = tamTamIds.getChatWithNonRemovableMsg();
        try {
            super.setQuery(botAPI.getAdmins(chatId));
        } catch (ClientException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void execute() {
        super.sendQuery();
    }
}

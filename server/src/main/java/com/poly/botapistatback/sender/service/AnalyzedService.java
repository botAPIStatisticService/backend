package com.poly.botapistatback.sender.service;

public interface AnalyzedService {
    void runQueries();
}

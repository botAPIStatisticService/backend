package com.poly.botapistatback.sender.service.tamtam;

import java.util.List;

import chat.tamtam.botapi.client.TamTamClient;

public interface TamTamCascadeQuery {

    void initialize(TamTamClient client);

    void execute();

    List<QueryResult> getResults();

    List<EndpointInfo> getEndpointInfos();
}

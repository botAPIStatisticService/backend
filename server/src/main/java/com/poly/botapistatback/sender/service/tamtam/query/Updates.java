package com.poly.botapistatback.sender.service.tamtam.query;

import org.springframework.stereotype.Component;

import com.poly.botapistatback.sender.service.tamtam.AbstractTamTamQuery;

import chat.tamtam.botapi.TamTamBotAPI;

@Component
public class Updates extends AbstractTamTamQuery {

    private static final String endpointName = "/updates";

    private static final String method = "GET";

    public Updates() {
        super(endpointName, method);
    }
    @Override
    public void initialize(TamTamBotAPI botAPI) {
        super.setQuery(botAPI.getUpdates());
    }

    @Override
    public void execute() {
        super.sendQuery();
    }
}

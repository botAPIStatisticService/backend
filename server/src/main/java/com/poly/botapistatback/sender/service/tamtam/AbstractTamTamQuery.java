package com.poly.botapistatback.sender.service.tamtam;

import java.util.concurrent.Future;

import chat.tamtam.botapi.TamTamBotAPI;
import chat.tamtam.botapi.exceptions.ClientException;
import chat.tamtam.botapi.model.TamTamSerializable;
import chat.tamtam.botapi.queries.TamTamQuery;

public abstract class AbstractTamTamQuery {

    private String localEN, localM;
    private Future<? extends TamTamSerializable> response;

    private TamTamQuery<? extends TamTamSerializable> query;

    protected AbstractTamTamQuery(String endpointName, String method) {
        localEN = endpointName;
        localM = method;
    }
    abstract public void initialize(TamTamBotAPI botAPI);

    protected void setQuery(TamTamQuery<? extends TamTamSerializable> query) {
        this.query = query;
    }
    abstract public void execute();

    protected void sendQuery() {
        try {
            response = query.enqueue();
        } catch (ClientException e) {
            throw new RuntimeException(e);
        }
    }
    public QueryResult getResult() {
        if (response == null || !response.isDone()) {
            return null;
        }
        try {
            response.get();
            return new QueryResult(true, localEN, localM);
        } catch (Exception e) {
            return new QueryResult(false, localEN, localM);
        }
    }
    public EndpointInfo getEndpointInfo() {
        return new EndpointInfo(localEN, localM);
    }

    public boolean isDone() {
//        if (response != null) {
//            System.out.println(localM + ":" + localEN + "| " + (response.isDone() ? "Done" : "Process"));
//        }
        return response == null || response.isDone();
    }

    protected Future<? extends TamTamSerializable> getFuture() {
        return response;
    }
}

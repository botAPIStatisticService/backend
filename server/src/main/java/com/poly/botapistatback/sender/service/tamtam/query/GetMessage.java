package com.poly.botapistatback.sender.service.tamtam.query;

import org.springframework.stereotype.Component;

import com.poly.botapistatback.sender.service.tamtam.AbstractTamTamQuery;
import com.poly.botapistatback.sender.service.tamtam.TamTamIds;

import chat.tamtam.botapi.TamTamBotAPI;
import chat.tamtam.botapi.exceptions.ClientException;

@Component
public class GetMessage extends AbstractTamTamQuery {

    private static final String endpointName = "/messages/{messageId}";

    private static final String method = "GET";

    final private TamTamIds tamTamIds;

    public GetMessage(TamTamIds tamTamIds) {
        super(endpointName, method);
        this.tamTamIds = tamTamIds;
    }
    @Override
    public void initialize(TamTamBotAPI botAPI) {
        String messageId = tamTamIds.getNonRemovableMsgId();
        try {
            super.setQuery(botAPI.getMessageById(messageId));
        } catch (ClientException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void execute() {
        super.sendQuery();
    }
}

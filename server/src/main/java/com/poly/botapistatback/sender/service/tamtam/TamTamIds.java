package com.poly.botapistatback.sender.service.tamtam;

import org.springframework.stereotype.Component;

@Component
public class TamTamIds {
    private Long chatWithNonRemovableMsg;
    private Long chatWithRemovableMsg;
    private Long testUserId;
    private String token;
    private String nonRemovableMsgId;

    private String name;
    private String chatLink;

    private String subscriptionUrl;

    public Long getChatWithNonRemovableMsg() {
        return chatWithNonRemovableMsg;
    }

    public void setChatWithNonRemovableMsg(Long chatWithNonRemovableMsg) {
        this.chatWithNonRemovableMsg = chatWithNonRemovableMsg;
    }

    public Long getChatWithRemovableMsg() {
        return chatWithRemovableMsg;
    }

    public void setChatWithRemovableMsg(Long chatWithRemovableMsg) {
        this.chatWithRemovableMsg = chatWithRemovableMsg;
    }

    public Long getTestUserId() {
        return testUserId;
    }

    public void setTestUserId(Long testUserId) {
        this.testUserId = testUserId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getNonRemovableMsgId() {
        return nonRemovableMsgId;
    }

    public void setNonRemovableMsgId(String nonRemovableMsgId) {
        this.nonRemovableMsgId = nonRemovableMsgId;
    }

    public String getChatLink() {
        return chatLink;
    }

    public void setChatLink(String chatLink) {
        this.chatLink = chatLink;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubscriptionUrl() {
        return subscriptionUrl;
    }

    public void setSubscriptionUrl(String subscriptionUrl) {
        this.subscriptionUrl = subscriptionUrl;
    }
}

package com.poly.botapistatback.sender.service.tamtam.query;

import org.springframework.stereotype.Component;

import com.poly.botapistatback.sender.service.tamtam.AbstractTamTamQuery;
import com.poly.botapistatback.sender.service.tamtam.TamTamIds;

import chat.tamtam.botapi.TamTamBotAPI;
import chat.tamtam.botapi.exceptions.ClientException;

@Component
public class RemoveMember extends AbstractTamTamQuery {

    private static final String endpointName = "/chats/{chatId}/members";

    private static final String method = "DELETE";

    final private TamTamIds tamTamIds;

    public RemoveMember(TamTamIds tamTamIds) {
        super(endpointName, method);
        this.tamTamIds = tamTamIds;
    }
    @Override
    public void initialize(TamTamBotAPI botAPI) {
        Long chatId = tamTamIds.getChatWithNonRemovableMsg();
        Long userId = tamTamIds.getTestUserId();
        try {
            super.setQuery(botAPI.removeMember(chatId, userId));
        } catch (ClientException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void execute() {
        super.sendQuery();
    }
}

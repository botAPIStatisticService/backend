package com.poly.botapistatback.sender;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.poly.botapistatback.sender.service.tamtam.BotApiService;

import chat.tamtam.botapi.TamTamBotAPI;
import chat.tamtam.botapi.model.BotInfo;
import chat.tamtam.botapi.queries.GetMyInfoQuery;

@Component
public class Sender implements InitializingBean {

    @Autowired
    BotApiService botApiService;

    @Override
    public void afterPropertiesSet() throws Exception {
        ScheduledThreadPoolExecutor timer = new ScheduledThreadPoolExecutor(1);
        timer.scheduleAtFixedRate(() -> {
            try {
                botApiService.runQueries();
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }, 0, 1, TimeUnit.SECONDS);
    }
}

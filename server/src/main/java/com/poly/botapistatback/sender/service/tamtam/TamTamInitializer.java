package com.poly.botapistatback.sender.service.tamtam;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.poly.botapistatback.sender.service.tamtam.query.GetChatById;
import com.poly.botapistatback.sender.service.tamtam.query.GetChatByLink;
import com.poly.botapistatback.sender.service.tamtam.query.GetChats;
import com.poly.botapistatback.sender.service.tamtam.query.SendMessage;

import chat.tamtam.botapi.TamTamBotAPI;
import chat.tamtam.botapi.client.TamTamTransportClient;
import chat.tamtam.botapi.client.impl.OkHttpTransportClient;
import chat.tamtam.botapi.model.Chat;
import chat.tamtam.botapi.model.ChatList;
import chat.tamtam.botapi.model.ChatType;
import okhttp3.OkHttpClient;
import okhttp3.Response;

@Component
@PropertySource("classpath:application.properties")
public class TamTamInitializer {

    final
    Environment environment;

    private String token;
    private TamTamBotAPI botAPI;

    private List<Chat> chats = new ArrayList<>();
    private final GetChats getChats;

    private final SendMessage sendMessage;

    private final TamTamIds tamTamIds;

    private Chat chatForUserId;

    private final GetChatByLink getChatByLink;

    private final GetChatById getChatById;

    public TamTamInitializer(GetChats getChats,
                             SendMessage sendMessage,
                             TamTamIds tamTamIds,
                             Environment environment,
                             GetChatByLink getChatByLink,
                             GetChatById getChatById) {
        this.getChats = getChats;
        this.sendMessage = sendMessage;
        this.tamTamIds = tamTamIds;
        this.environment = environment;
        this.getChatByLink = getChatByLink;
        this.getChatById = getChatById;
    }

    public void initialize() {
        String token = environment.getProperty("tamtam.token");
        if (token == null) {
            throw new RuntimeException("Token not configured");
        }
        this.token = token;
        tamTamIds.setToken(token);
        String name = environment.getProperty("tamtam.name");
        if (name == null) {
            throw new RuntimeException("TamTam service name not configured");
        }
        String subUrl = environment.getProperty("tamtam.subscription.url");
        if (subUrl == null) {
            throw new RuntimeException("TamTam subscription url not configured");
        }
        tamTamIds.setSubscriptionUrl(subUrl);
        tamTamIds.setName(name);
        botAPI = TamTamBotAPI.create(token);
        String initType = environment.getProperty("tamtam.chat.init.type");
        if (initType == null) {
            initType = "auto";
        }
        switch (initType) {
            case "auto":
                autoInit();
                break;
            case "link":
                linkInit();
                break;
            case "id":
                idInit();
            default:
                throw new RuntimeException("Incorrect property: tamtam.chat.init.type");
        }
        String userIdInitType = environment.getProperty("tamtam.user.init.type");
        if (userIdInitType == null) {
            userIdInitType = "auto";
        }
        switch (userIdInitType) {
            case "auto":
                autoInitUserId();
                break;
            case "manual":
                manualInitUserId();
                break;
            default:
                throw new RuntimeException("Incorrect property: tamtam.user.init.type");
        }
    }

    private void autoInit() {
        if (!checkChats()) {
            throw new RuntimeException("Chat count check fail");
        }

        Chat nonRemovable = chats.get(0);
        if (nonRemovable.getLink() == null && nonRemovable.getLink().split("/")[3].equals("join")) {
            throw new RuntimeException("Chat has no public link");
        }
        String link = nonRemovable.getLink().split("/")[3];
        tamTamIds.setChatLink(link);
        tamTamIds.setChatWithNonRemovableMsg(nonRemovable.getChatId());
        sendMessage.initialize(botAPI, tamTamIds.getChatWithNonRemovableMsg());
        sendMessage.execute();
        if (!sendMessage.getResult().getSuccess()) {
            throw new RuntimeException("Can't send message to first chat");
        }
        String nonRemovableMsgId = sendMessage.getMessageId();
        tamTamIds.setNonRemovableMsgId(nonRemovableMsgId);
        chatForUserId = nonRemovable;
        Chat removable = chats.get(1);
        tamTamIds.setChatWithRemovableMsg(removable.getChatId());

    }

    private Boolean checkChats() {
        getChats.initialize(botAPI);
        getChats.execute();
        try {
            ChatList chatList = getChats.getData();
            for (Chat chat : chatList.getChats()) {
                if (chat.getType() == ChatType.CHAT) {
                    chats.add(chat);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException("Failed receiving chats information");
        }
        return chats.size() >= 2;
    }

    private void linkInit() {
        String firstLink = environment.getProperty("tamtam.chat.init.first.link");
        if (firstLink == null) {
            throw new RuntimeException("tamtam.chat.init.first.link: not configured");
        }
        String secondLink = environment.getProperty("tamtam.chat.init.second.link");
        if (secondLink == null) {
            throw new RuntimeException("tamtam.chat.init.second.link: not configured");
        }
        if (firstLink.equals(secondLink)) {
            throw new RuntimeException("tamtam.chat.init: You must define two different chats");
        }
        if (firstLink.startsWith("https:")) {
            firstLink = firstLink.split("/")[3];
        }
        if (secondLink.startsWith("https:")) {
            secondLink = secondLink.split("/")[3];
        }
        tamTamIds.setChatLink(firstLink);
        getChatByLink.initialize(botAPI);
        getChatByLink.execute();
        if (!getChatByLink.getResult().getSuccess()) {
            throw new RuntimeException("tamtam.chat.init.first: Failed to receive chat info");
        }
        Chat firstChat = getChatByLink.getData();
        tamTamIds.setChatWithRemovableMsg(firstChat.getChatId());
        tamTamIds.setChatLink(secondLink);
        getChatByLink.initialize(botAPI);
        getChatByLink.execute();
        if (!getChatByLink.getResult().getSuccess()) {
            throw new RuntimeException("tamtam.chat.init.second: Failed to receive chat info");
        }
        Chat secondChat = getChatByLink.getData();
        chatForUserId = secondChat;
        tamTamIds.setChatWithNonRemovableMsg(secondChat.getChatId());
    }

    private void idInit() {
        String firstIdStr = environment.getProperty("tamtam.chat.init.first.id");
        if (firstIdStr == null) {
            throw new RuntimeException("tamtam.chat.init.first.id: not configured");
        }
        String secondIdStr = environment.getProperty("tamtam.chat.init.second.id");
        if (secondIdStr == null) {
            throw new RuntimeException("tamtam.chat.init.second.id: not configured");
        }
        long firstId;
        try {
            firstId = Long.parseLong(firstIdStr);
        } catch (NumberFormatException e) {
            throw new RuntimeException("tamtam.chat.init.first.id: cannot parse number");
        }
        long secondId;
        try {
            secondId = Long.parseLong(secondIdStr);
        } catch (NumberFormatException e) {
            throw new RuntimeException("tamtam.chat.init.second.id: cannot parse number");
        }
        tamTamIds.setChatWithRemovableMsg(firstId);
        tamTamIds.setChatWithNonRemovableMsg(secondId);
        getChatById.initialize(botAPI, firstId);
        getChatById.execute();
        if (!getChatById.getResult().getSuccess()) {
            throw new RuntimeException("tamtam.chat.init.first: Failed to receive chat info");
        }
        getChatById.initialize(botAPI, secondId);
        getChatById.execute();
        if (!getChatById.getResult().getSuccess()) {
            throw new RuntimeException("tamtam.chat.init.second: Failed to receive chat info");
        }
        Chat secondChat = getChatById.getData();
        String chatLink = secondChat.getLink();
        if (chatLink == null || chatLink.split("/")[3].equals("join")) {
            throw new RuntimeException("tamtam.chat.init.second: Can't get chat public link");
        }
        chatLink = chatLink.split("/")[3];
        tamTamIds.setChatLink(chatLink);
        chatForUserId = secondChat;
    }

    private void autoInitUserId() {
        tamTamIds.setTestUserId(chatForUserId.getOwnerId());
    }

    private void manualInitUserId() {
        String userIdStr = environment.getProperty("tamtam.user.init.id");
        if (userIdStr == null) {
            throw new RuntimeException("tamtam.user.init.id: not configured");
        }
        long userId;
        try {
            userId = Long.parseLong(userIdStr);
        } catch (NumberFormatException e) {
            throw new RuntimeException("tamtam.user.init.id: cannot parse number");
        }
        tamTamIds.setTestUserId(userId);
    }

}

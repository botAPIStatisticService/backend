package com.poly.botapistatback.sender.service.tamtam.query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.poly.botapistatback.sender.service.tamtam.AbstractTamTamQuery;
import com.poly.botapistatback.sender.service.tamtam.TamTamIds;

import chat.tamtam.botapi.TamTamBotAPI;
import chat.tamtam.botapi.exceptions.ClientException;

@Component
public class GetChatMembership extends AbstractTamTamQuery {

    private static final String endpointName = "/chats/{chatId}/members/me";

    private static final String method = "GET";

    @Autowired
    TamTamIds tamTamIds;

    public GetChatMembership() {
        super(endpointName, method);
    }
    @Override
    public void initialize(TamTamBotAPI botAPI) {
        Long chatId = tamTamIds.getChatWithNonRemovableMsg();
        try {
            super.setQuery(botAPI.getMembership(chatId));
        } catch (ClientException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void execute() {
        super.sendQuery();
    }
}

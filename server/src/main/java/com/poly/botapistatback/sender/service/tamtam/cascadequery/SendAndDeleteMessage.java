package com.poly.botapistatback.sender.service.tamtam.cascadequery;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.poly.botapistatback.sender.service.tamtam.EndpointInfo;
import com.poly.botapistatback.sender.service.tamtam.QueryResult;
import com.poly.botapistatback.sender.service.tamtam.TamTamCascadeQuery;
import com.poly.botapistatback.sender.service.tamtam.TamTamIds;
import com.poly.botapistatback.sender.service.tamtam.query.DeleteMessage;
import com.poly.botapistatback.sender.service.tamtam.query.EditMessage;
import com.poly.botapistatback.sender.service.tamtam.query.SendMessage;

import chat.tamtam.botapi.TamTamBotAPI;
import chat.tamtam.botapi.client.TamTamClient;
import chat.tamtam.botapi.model.SendMessageResult;
import chat.tamtam.botapi.model.SimpleQueryResult;

@Component
public class SendAndDeleteMessage implements TamTamCascadeQuery {

    final
    SendMessage sendMessage;

    final
    DeleteMessage deleteMessage;

    final EditMessage editMessage;

    private TamTamBotAPI botAPI;

    private QueryResult sendMessageResult;
    private QueryResult deleteMessageResult;

    private QueryResult editMessageResult;

    private boolean editMessageSuccess = false;

    private String messageId;

    private String editedMessageId;

    @Autowired
    TamTamIds tamTamIds;

    public SendAndDeleteMessage(SendMessage sendMessage, DeleteMessage deleteMessage, EditMessage editMessage) {
        this.editMessage = editMessage;
        this.sendMessage = sendMessage;
        this.deleteMessage = deleteMessage;
        sendMessageResult = new QueryResult(true, null, null);
    }


    @Override
    public void initialize(TamTamClient client) {
        botAPI = new TamTamBotAPI(client);
        sendMessage.initialize(botAPI, tamTamIds.getChatWithRemovableMsg());
        deleteMessage.initialize(botAPI, "");
    }

    @Override
    public void execute() {
        sendMessageResult = null;
        deleteMessageResult = null;
        editMessageResult = null;
        Future<SimpleQueryResult> deleteFuture = deleteMessage.getFutureResult();
        if (deleteFuture != null && deleteFuture.isDone()) {
            deleteMessageResult = deleteMessage.getResult();
        }
        if (editedMessageId != null && editMessageSuccess && (deleteFuture == null || deleteFuture.isDone())) {
            deleteMessage.initialize(botAPI, editedMessageId);
            deleteMessage.execute();
            editedMessageId = null;
            editMessageSuccess = false;
        }
        Future<SimpleQueryResult> editFuture = editMessage.getFutureResult();
        if (editFuture != null && editFuture.isDone()) {
            editMessageResult = editMessage.getResult();
            if (editMessageResult.getSuccess()) {
                editMessageSuccess = true;
            }
        }
        if (messageId != null && (editFuture == null || editFuture.isDone())) {
            editMessage.initialize(botAPI, messageId);
            editMessage.execute();
            editedMessageId = messageId;
            messageId = null;
        }
        Future<SendMessageResult> sendFuture = sendMessage.getFutureResult();
        if (sendFuture != null && sendFuture.isDone()) {
            sendMessageResult = sendMessage.getResult();
            if (sendMessageResult.getSuccess()) {
                messageId = sendMessage.getMessageId();
            }
        }
        if (sendFuture == null || sendFuture.isDone()) {
            System.out.println("Send Execute");
            sendMessage.execute();
        }
    }

    @Override
    public List<QueryResult> getResults() {
        return Arrays.asList(sendMessageResult, editMessageResult, deleteMessageResult);
    }

    @Override
    public List<EndpointInfo> getEndpointInfos() {
        return List.of(sendMessage.getEndpointInfo(), editMessage.getEndpointInfo(), deleteMessage.getEndpointInfo());
    }
}

package com.poly.botapistatback.sender.service.tamtam.query;

import org.springframework.stereotype.Component;

import com.poly.botapistatback.sender.service.tamtam.AbstractTamTamQuery;

import chat.tamtam.botapi.TamTamBotAPI;

@Component
public class GetSubscriptions extends AbstractTamTamQuery {

    private static final String endpointName = "/subscriptions";

    private static final String method = "GET";

    public GetSubscriptions() {
        super(endpointName, method);
    }
    @Override
    public void initialize(TamTamBotAPI botAPI) {
        super.setQuery(botAPI.getSubscriptions());
    }

    @Override
    public void execute() {
        super.sendQuery();
    }
}

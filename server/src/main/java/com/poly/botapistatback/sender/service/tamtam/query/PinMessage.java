package com.poly.botapistatback.sender.service.tamtam.query;

import org.springframework.stereotype.Component;

import com.poly.botapistatback.sender.service.tamtam.AbstractTamTamQuery;
import com.poly.botapistatback.sender.service.tamtam.TamTamIds;

import chat.tamtam.botapi.TamTamBotAPI;
import chat.tamtam.botapi.exceptions.ClientException;
import chat.tamtam.botapi.model.PinMessageBody;

@Component
public class PinMessage extends AbstractTamTamQuery {

    private static final String endpointName = "/chats/{chatId}/pin";

    private static final String method = "PUT";

    final private TamTamIds tamTamIds;

    public PinMessage(TamTamIds tamTamIds) {
        super(endpointName, method);
        this.tamTamIds = tamTamIds;
    }

    @Override
    public void initialize(TamTamBotAPI botAPI) {
        Long chatId = tamTamIds.getChatWithNonRemovableMsg();
        String messageId = tamTamIds.getNonRemovableMsgId();
        PinMessageBody pinMessageBody = new PinMessageBody(messageId);
        try {
            super.setQuery(botAPI.pinMessage(pinMessageBody, chatId));
        } catch (ClientException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void execute() {
        super.sendQuery();
    }
}

package com.poly.botapistatback.sender.service.tamtam;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import com.poly.botapistatback.sender.service.AnalyzedService;
import com.poly.botapistatback.service.EndpointService;
import com.poly.botapistatback.service.ProbeService;
import com.poly.botapistatback.service.ServiceService;

import chat.tamtam.botapi.TamTamBotAPI;
import chat.tamtam.botapi.client.TamTamClient;
import chat.tamtam.botapi.client.TamTamSerializer;
import chat.tamtam.botapi.client.TamTamTransportClient;
import chat.tamtam.botapi.client.impl.JacksonSerializer;
import chat.tamtam.botapi.client.impl.OkHttpTransportClient;
import okhttp3.Dispatcher;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;


@Component
public class BotApiService implements AnalyzedService {
    private String name;
    private String token;
    final private TamTamBotAPI botAPI;
    private Integer serviceId;

    private final
    ServiceService serviceService;

    private final
    EndpointService endpointService;

    private final
    ProbeService probeService;

    private final
    List<AbstractTamTamQuery> queryList;

    private final List<TamTamCascadeQuery> cascadeQueryList;

    private final TamTamInitializer tamTamInitializer;

    private final TamTamIds tamTamIds;

    private final EndpointNameConverter endpointNameConverter;

    public BotApiService(ServiceService serviceService, EndpointService endpointService,
                         ProbeService probeService, List<AbstractTamTamQuery> queryList,
                         List<TamTamCascadeQuery> cascadeQueryList, TamTamInitializer tamTamInitializer,
                         TamTamIds tamTamIds, EndpointNameConverter endpointNameConverter) {
        //Initialize sender
        tamTamInitializer.initialize();
        this.name = tamTamIds.getName();
        try {
            serviceId = serviceService.getServiceByName(name).getId();
        } catch (Exception e) {
            serviceId = serviceService.insertNewService(name);
        }
        this.token = tamTamIds.getToken();

        //Create dispatcher and set maxRequests to 100
        Dispatcher dispatcher = new Dispatcher();
        dispatcher.setMaxRequests(100);
        dispatcher.setMaxRequestsPerHost(100);
        //Create client and botApi
        TamTamTransportClient transportClient = new OkHttpTransportClient(new OkHttpClient.Builder()
                .addInterceptor(this::writeResponseInfo)
                .readTimeout(100, TimeUnit.SECONDS)
                .callTimeout(100, TimeUnit.SECONDS)
                .dispatcher(dispatcher)
                .followRedirects(true)
                .build());
        TamTamSerializer serializer = new JacksonSerializer();
        TamTamClient client = new TamTamClient(token, transportClient, serializer);
        botAPI = new TamTamBotAPI(client);
        //Initialize simple queries
        for (AbstractTamTamQuery query : queryList) {
            query.initialize(botAPI);
            EndpointInfo endpointInfo = query.getEndpointInfo();
            Integer endpointId = endpointService.getEndpointId(
                    endpointInfo.getEndpointName(),
                    endpointInfo.getMethod(),
                    serviceId);
            if (endpointId == null) {
                endpointService.insertNewEndpoint(
                        endpointInfo.getEndpointName(),
                        endpointInfo.getMethod(),
                        serviceId);
            }
        }
        //Initialize cascade queries
        for (TamTamCascadeQuery query : cascadeQueryList) {
            query.initialize(client);
            for (EndpointInfo endpointInfo : query.getEndpointInfos()) {
                Integer endpointId = endpointService.getEndpointId(
                        endpointInfo.getEndpointName(),
                        endpointInfo.getMethod(),
                        serviceId
                );
                if (endpointId == null) {
                    endpointService.insertNewEndpoint(
                            endpointInfo.getEndpointName(),
                            endpointInfo.getMethod(),
                            serviceId
                    );
                }
            }
        }

        this.serviceService = serviceService;
        this.endpointService = endpointService;
        this.probeService = probeService;
        this.queryList = queryList;
        this.cascadeQueryList = cascadeQueryList;
        this.tamTamInitializer = tamTamInitializer;
        this.tamTamIds = tamTamIds;
        this.endpointNameConverter = endpointNameConverter;
    }

    @NotNull
    private Response writeResponseInfo(Interceptor.Chain chain) throws IOException {
        String endpointName = chain.request().url().url().getPath();
        String method = chain.request().method();
        endpointName = endpointNameConverter.convert(endpointName);
//        long sendTime = LocalDateTime.now().toEpochSecond(ZoneOffset.UTC);
        Response response;
        try {
            response = chain.proceed(chain.request());
        } catch (Exception e) {
            probeService.insertNewProbe(endpointName, method,
                            serviceId, 0, false, 0, null);
            throw e;
        }
        long time_diff = response.receivedResponseAtMillis() - response.sentRequestAtMillis();
//        System.out.println(endpointName + " : " + time_diff + "ms;" + " Response code: " + response.code());
        boolean success = response.code() >= 200 && response.code() < 400;
        probeService.insertNewProbe(endpointName, method, serviceId,
                success ? (int) time_diff : 0, success, response.code(),
                LocalDateTime.now(ZoneOffset.UTC).toEpochSecond(ZoneOffset.UTC));
        return response;
    }

    @Override
    public void runQueries() {
        for (AbstractTamTamQuery query : queryList) {
            if (query.isDone()) {
                query.execute();
            }
        }
        for (TamTamCascadeQuery query : cascadeQueryList) {
            query.execute();
        }
    }
}

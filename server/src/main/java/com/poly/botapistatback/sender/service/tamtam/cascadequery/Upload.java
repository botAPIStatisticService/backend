package com.poly.botapistatback.sender.service.tamtam.cascadequery;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Future;

import org.springframework.stereotype.Component;

import com.poly.botapistatback.sender.service.tamtam.EndpointInfo;
import com.poly.botapistatback.sender.service.tamtam.QueryResult;
import com.poly.botapistatback.sender.service.tamtam.TamTamCascadeQuery;
import com.poly.botapistatback.sender.service.tamtam.query.GetUploadUrl;
import com.poly.botapistatback.sender.service.tamtam.query.UploadImage;

import chat.tamtam.botapi.TamTamBotAPI;
import chat.tamtam.botapi.TamTamUploadAPI;
import chat.tamtam.botapi.client.TamTamClient;
import chat.tamtam.botapi.model.PhotoTokens;
import chat.tamtam.botapi.model.UploadEndpoint;

@Component
public class Upload implements TamTamCascadeQuery {

    final private GetUploadUrl getUploadUrl;

    final private UploadImage uploadImage;

    private QueryResult urlResult;
    private QueryResult uploadResult;

    private String uploadUrl;

    public Upload(GetUploadUrl getUploadUrl, UploadImage uploadImage) {
        this.getUploadUrl = getUploadUrl;
        this.uploadImage = uploadImage;
        urlResult = new QueryResult(true, null, null);
    }

    @Override
    public void initialize(TamTamClient client) {
        TamTamBotAPI botAPI = new TamTamBotAPI(client);
        TamTamUploadAPI uploadAPI = new TamTamUploadAPI(client);
        uploadImage.setUploadApi(uploadAPI);
        getUploadUrl.initialize(botAPI);
        uploadImage.initialize("UploadImage");
    }

    @Override
    public void execute() {
        urlResult = null;
        uploadResult = null;
        Future<PhotoTokens> uploadFuture = uploadImage.getFutureResult();
        if (uploadFuture != null && uploadFuture.isDone()) {
            uploadResult = uploadImage.getResult();
        }
        if (uploadUrl != null && (uploadFuture == null || uploadFuture.isDone())) {
            uploadImage.initialize(uploadUrl);
            uploadImage.execute();
            uploadUrl = null;
        }
        Future<UploadEndpoint> urlFuture = getUploadUrl.getFutureResult();
        if (urlFuture != null && urlFuture.isDone()) {
            urlResult = getUploadUrl.getResult();
            if (urlResult.getSuccess()) {
                uploadUrl = getUploadUrl.getUploadUrl();
            }
        }
        if (urlFuture == null || urlFuture.isDone()) {
            getUploadUrl.execute();
        }
    }

    @Override
    public List<QueryResult> getResults() {
        return Arrays.asList(urlResult, uploadResult);
    }

    @Override
    public List<EndpointInfo> getEndpointInfos() {
        return List.of(getUploadUrl.getEndpointInfo(), uploadImage.getEndpointInfo());
    }
}

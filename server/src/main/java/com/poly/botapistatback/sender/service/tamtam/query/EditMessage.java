package com.poly.botapistatback.sender.service.tamtam.query;

import java.util.concurrent.Future;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Component;

import com.poly.botapistatback.sender.service.tamtam.AbstractTamTamQuery;
import com.poly.botapistatback.sender.service.tamtam.EndpointInfo;
import com.poly.botapistatback.sender.service.tamtam.QueryResult;
import com.poly.botapistatback.sender.service.tamtam.TamTamIds;

import chat.tamtam.botapi.TamTamBotAPI;
import chat.tamtam.botapi.exceptions.ClientException;
import chat.tamtam.botapi.model.NewMessageBody;
import chat.tamtam.botapi.model.SimpleQueryResult;
import chat.tamtam.botapi.queries.DeleteMessageQuery;
import chat.tamtam.botapi.queries.EditMessageQuery;

@Component
public class EditMessage {

    private static final String endpointName = "/messages";

    private static final String method = "PUT";

    private EditMessageQuery editMessageQuery;

    private Future<SimpleQueryResult> response;

    public void initialize(TamTamBotAPI botAPI, String messageId) {
        String message = RandomStringUtils.random(10, true, false);
        NewMessageBody newMessageBody = new NewMessageBody(message, null, null);
        try {
            editMessageQuery = botAPI.editMessage(newMessageBody, messageId);
        } catch (ClientException e) {
            throw new RuntimeException(e);
        }
    }

    public void execute() {
        try {
            response = editMessageQuery.enqueue();
        } catch (ClientException e) {
            throw new RuntimeException(e);
        }
    }

    public QueryResult getResult() {
        try {
            response.get();
            return new QueryResult(true, endpointName, method);
        } catch (Exception e) {
            return new QueryResult(false, endpointName, method);
        }
    }

    public EndpointInfo getEndpointInfo() {
        return new EndpointInfo(endpointName, method);
    }

    public Future<SimpleQueryResult> getFutureResult() {
        return response;
    }
}

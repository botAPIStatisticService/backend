package com.poly.botapistatback.sender.service.tamtam.query;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Component;

import com.poly.botapistatback.sender.service.tamtam.AbstractTamTamQuery;

import chat.tamtam.botapi.TamTamBotAPI;
import chat.tamtam.botapi.exceptions.ClientException;
import chat.tamtam.botapi.model.BotPatch;

@Component
public class PatchMe extends AbstractTamTamQuery {

    private static final String endpointName = "/me";

    private static final String method = "PATCH";

    private TamTamBotAPI botAPI;

    public PatchMe() {
        super(endpointName, method);
    }
    @Override
    public void initialize(TamTamBotAPI botAPI) {
        this.botAPI = botAPI;
        BotPatch botPatch = new BotPatch();
        botPatch.setName("Booot");
        botPatch.setDescription("This is bot");
        try {
            super.setQuery(botAPI.editMyInfo(botPatch));
        } catch (ClientException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void execute() {
        BotPatch botPatch = new BotPatch();
        botPatch.setName(RandomStringUtils.random(10, true, false));
        try {
            super.setQuery(botAPI.editMyInfo(botPatch));
            super.sendQuery();
        } catch (ClientException e) {
            throw new RuntimeException(e);
        }
    }
}

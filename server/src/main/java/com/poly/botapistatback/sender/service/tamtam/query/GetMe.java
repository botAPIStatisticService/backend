package com.poly.botapistatback.sender.service.tamtam.query;

import org.springframework.stereotype.Component;

import com.poly.botapistatback.sender.service.tamtam.AbstractTamTamQuery;

import chat.tamtam.botapi.TamTamBotAPI;

@Component
public class GetMe extends AbstractTamTamQuery {

    private static final String endpointName = "/me";

    private static final String method = "GET";

    public GetMe() {
        super(endpointName, method);
    }
    @Override
    public void initialize(TamTamBotAPI botAPI) {
        super.setQuery(botAPI.getMyInfo());
    }

    @Override
    public void execute() {
        super.sendQuery();
    }
}

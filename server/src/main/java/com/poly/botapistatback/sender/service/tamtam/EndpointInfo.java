package com.poly.botapistatback.sender.service.tamtam;

public class EndpointInfo {
    private String endpointName;
    private String method;

    public EndpointInfo(String endpointName, String method) {
        this.endpointName = endpointName;
        this.method = method;
    }

    public String getEndpointName() {
        return endpointName;
    }

    public void setEndpointName(String endpointName) {
        this.endpointName = endpointName;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }
}

package com.poly.botapistatback.sender.service.tamtam.query;

import java.util.concurrent.ExecutionException;

import org.springframework.stereotype.Component;

import com.poly.botapistatback.sender.service.tamtam.AbstractTamTamQuery;

import chat.tamtam.botapi.TamTamBotAPI;
import chat.tamtam.botapi.model.ChatList;

@Component
public class GetChats extends AbstractTamTamQuery {

    private static final String endpointName = "/chats";

    private static final String method = "GET";

    public GetChats() {
        super(endpointName, method);
    }

    @Override
    public void initialize(TamTamBotAPI botAPI) {
        super.setQuery(botAPI.getChats());
    }

    @Override
    public void execute() {
        super.sendQuery();
    }

    public ChatList getData() throws ExecutionException, InterruptedException {
        return (ChatList) super.getFuture().get();
    }
}

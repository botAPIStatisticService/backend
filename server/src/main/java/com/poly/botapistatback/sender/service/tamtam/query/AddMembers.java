package com.poly.botapistatback.sender.service.tamtam.query;

import java.util.List;

import org.springframework.stereotype.Component;

import com.poly.botapistatback.sender.service.tamtam.AbstractTamTamQuery;
import com.poly.botapistatback.sender.service.tamtam.TamTamIds;

import chat.tamtam.botapi.TamTamBotAPI;
import chat.tamtam.botapi.exceptions.ClientException;
import chat.tamtam.botapi.model.UserIdsList;

@Component
public class AddMembers extends AbstractTamTamQuery {

    private static final String method = "POST";

    private static final String endpointName = "/chats/{chatId}/members";

    final private TamTamIds tamTamIds;

    public AddMembers(TamTamIds tamTamIds) {
        super(endpointName, method);
        this.tamTamIds = tamTamIds;
    }

    @Override
    public void initialize(TamTamBotAPI botAPI) {
        Long chatId = tamTamIds.getChatWithNonRemovableMsg();
        Long userId = tamTamIds.getTestUserId();
        UserIdsList userIdsList = new UserIdsList(List.of(userId));
        try {
            super.setQuery(botAPI.addMembers(userIdsList, chatId));
        } catch (ClientException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void execute() {
        super.sendQuery();
    }
}

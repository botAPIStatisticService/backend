package com.poly.botapistatback.sender.service.tamtam.query;

import java.io.InputStream;
import java.util.concurrent.Future;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.poly.botapistatback.sender.service.tamtam.EndpointInfo;
import com.poly.botapistatback.sender.service.tamtam.EndpointNameConverter;
import com.poly.botapistatback.sender.service.tamtam.QueryResult;

import chat.tamtam.botapi.TamTamUploadAPI;
import chat.tamtam.botapi.exceptions.ClientException;
import chat.tamtam.botapi.model.PhotoTokens;
import chat.tamtam.botapi.queries.upload.TamTamUploadImageQuery;

@Component
public class UploadImage {

    private static final String endpointName = "UploadImage";

    private static final String method = "POST";

    private TamTamUploadImageQuery uploadImageQuery;

    private Future<PhotoTokens> response;

    private TamTamUploadAPI uploadAPI;

    public void setUploadApi(TamTamUploadAPI uploadAPI) {
        this.uploadAPI = uploadAPI;
    }

    public void initialize(String uploadUrl) {
        InputStream in = this.getClass().getClassLoader().getResourceAsStream("static/pic.jpg");
        uploadImageQuery = uploadAPI.uploadImage(uploadUrl, "pic.jpg", in);
    }

    public void execute() {
        try {
            response = uploadImageQuery.enqueue();
        } catch (ClientException e) {
            throw new RuntimeException(e);
        }
    }

    public QueryResult getResult() {
        try {
            response.get();
            return new QueryResult(true, endpointName, method);
        } catch (Exception e) {
            return new QueryResult(false, endpointName, method);
        }
    }

    public EndpointInfo getEndpointInfo() {
        return new EndpointInfo(endpointName, method);
    }

    public Future<PhotoTokens> getFutureResult() {
        return response;
    }
}

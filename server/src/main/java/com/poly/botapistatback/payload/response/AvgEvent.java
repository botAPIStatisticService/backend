package com.poly.botapistatback.payload.response;

public class AvgEvent {
    Float successRate;
    Float averageResponseMS;
    Long bindPoint;

    public AvgEvent(Float successRate, Float averageResponseMS, Long bindPoint) {
        this.successRate = successRate;
        this.averageResponseMS = averageResponseMS;
        this.bindPoint = bindPoint;
    }

    public Float getSuccessRate() {
        return successRate;
    }

    public Float getAverageResponseMS() {
        return averageResponseMS;
    }

    public Long getBindPoint() {
        return bindPoint;
    }
}

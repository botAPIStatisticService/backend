package com.poly.botapistatback.payload.response;

public class Event {
    private Long id;
    private Boolean success;
    private Integer responseMS;

    private Integer responseCode;

    private Long requestTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getResponseMS() {
        return responseMS;
    }

    public void setResponseMS(Integer responseMS) {
        this.responseMS = responseMS;
    }

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public Long getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(Long requestTime) {
        this.requestTime = requestTime;
    }
}

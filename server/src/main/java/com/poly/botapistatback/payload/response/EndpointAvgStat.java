package com.poly.botapistatback.payload.response;

import java.util.List;

public class EndpointAvgStat {
    private Integer id;
    private String name;
    private String method;
    private List<AvgEvent> statistics;

    public EndpointAvgStat(Integer id, String name, String method, List<AvgEvent> statistics) {
        this.id = id;
        this.name = name;
        this.method = method;
        this.statistics = statistics;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getMethod() {
        return method;
    }

    public List<AvgEvent> getStatistics() {
        return statistics;
    }
}

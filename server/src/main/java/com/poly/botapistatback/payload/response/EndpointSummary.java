package com.poly.botapistatback.payload.response;

public class EndpointSummary {
    private Integer id;
    private String name;
    private Float successRate;
    private Integer averageResponseMS;

    private String method;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getSuccessRate() {
        return successRate;
    }

    public void setSuccessRate(Float successRate) {
        this.successRate = successRate;
    }

    public Integer getAverageResponseMS() {
        return averageResponseMS;
    }

    public void setAverageResponseMS(Integer averageResponseMS) {
        this.averageResponseMS = averageResponseMS;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }
}

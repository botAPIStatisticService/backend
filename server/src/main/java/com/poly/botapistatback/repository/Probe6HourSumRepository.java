package com.poly.botapistatback.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.poly.botapistatback.entity.ProbeDaySum;
import com.poly.botapistatback.entity.Probe6HourSum;

public interface Probe6HourSumRepository extends JpaRepository<Probe6HourSum, Long> {

    @Query("select s from Probe6HourSum s where s.endpoint.id = :endpointId and s.probeTruncTime >= :from " +
            "order by s.probeTruncTime asc")
    List<Probe6HourSum> getByEndpointFrom(@Param("endpointId") Integer endpointId, @Param("from") Long from);
}

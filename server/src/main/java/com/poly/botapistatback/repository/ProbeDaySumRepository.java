package com.poly.botapistatback.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.poly.botapistatback.entity.ProbeDaySum;

public interface ProbeDaySumRepository extends JpaRepository<ProbeDaySum, Long> {

    @Query("select s from ProbeDaySum s where s.endpoint.id = :endpointId and s.probeTruncTime >= :from " +
            "order by s.probeTruncTime asc")
    List<ProbeDaySum> getByEndpointFrom(@Param("endpointId") Integer endpointId, @Param("from") Long from);


}

package com.poly.botapistatback.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.poly.botapistatback.entity.Endpoint;
import com.poly.botapistatback.entity.Probe;

public interface ProbeRepository extends JpaRepository<Probe, Integer> {

    @Query(value = "select p from Probe p where p.endpoint.id = :endpoint_id order by p.probeTime desc")
    List<Probe> getProbesByEndpoint(@Param("endpoint_id") Integer endpoint_id, Pageable pageable);

    @Query(value = "select p from Probe p where p.endpoint.id = :endpoint_id and p.probeTime >= :from " +
            "order by p.probeTime asc")
    List<Probe> getProbesByEndpointFrom(@Param("endpoint_id") Integer endpoint_id, Long from);
}

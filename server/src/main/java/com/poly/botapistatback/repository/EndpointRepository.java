package com.poly.botapistatback.repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.lang.NonNullApi;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;

import com.poly.botapistatback.entity.Endpoint;
import com.poly.botapistatback.entity.Probe;
import com.poly.botapistatback.entity.Service;

public interface EndpointRepository extends JpaRepository<Endpoint, Integer> {
    @NotNull
    @Override
    Optional<Endpoint> findById(@NotNull Integer integer);

    @Query("select e from Endpoint e order by e.id asc")
    List<Endpoint> getAll();

    Optional<Endpoint> getByName(String name);

    @Query("select e from Endpoint e where e.service.id = :serviceId")
    List<Endpoint> getByServiceId(@Param("serviceId") Integer serviceId);

    Optional<Endpoint> getByNameAndMethodAndService(String name, String method, Service service);
}

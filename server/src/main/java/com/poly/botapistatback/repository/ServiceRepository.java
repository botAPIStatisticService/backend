package com.poly.botapistatback.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.poly.botapistatback.entity.Service;
import com.poly.botapistatback.entity.custom.EndpointAggregation;
import com.poly.botapistatback.entity.custom.ProbeSumAggregation;

public interface ServiceRepository extends JpaRepository<Service, Integer> {

    @Query("select new com.poly.botapistatback.entity.custom.EndpointAggregation" +
            "(p.endpoint.id, p.endpoint.name, avg(p.time), avg(p.success), p.endpoint.service.name, p.endpoint.method) " +
            "from Probe p where p.endpoint.service.id = :service_id and p.probeTime > :from " +
            "group by p.endpoint.id, p.endpoint.name, p.endpoint.service.name, p.endpoint.method " +
            "order by p.endpoint.id")
    List<EndpointAggregation> getEndpointAggregation(@Param("service_id") Integer service_id,
                                                     @Param("from") Long from);

    @Query("select new com.poly.botapistatback.entity.custom.EndpointAggregation" +
            "(p.endpoint.id, p.endpoint.name, avg(p.time), avg(p.success), p.endpoint.service.name, p.endpoint.method) " +
            "from Probe p where p.probeTime > :from " +
            "group by p.endpoint.id, p.endpoint.name, p.endpoint.service.name, p.endpoint.method " +
            "order by p.endpoint.id")
    List<EndpointAggregation> getAllEndpointAggregation(@Param("from") Long from);

    @Query("select new com.poly.botapistatback.entity.custom.ProbeSumAggregation" +
            "(p.endpoint.id, p.endpoint.name, p.endpoint.method, " +
            "p.endpoint.service.name, sum(p.sumTime), sum(p.successCount), sum(p.count)) " +
            "from ProbeDaySum p where p.probeTruncTime >= :from " +
            "group by p.endpoint.id, p.endpoint.name, p.endpoint.service.name, p.endpoint.method " +
            "order by p.endpoint.id")
    List<ProbeSumAggregation> getAllSumEndpointAggregation(@Param("from") Long from);

    @Query("select new com.poly.botapistatback.entity.custom.ProbeSumAggregation" +
            "(p.endpoint.id, p.endpoint.name, p.endpoint.method, " +
            "p.endpoint.service.name, sum(p.sumTime), sum(p.successCount), sum(p.count)) " +
            "from ProbeDaySum p where p.probeTruncTime >= :from and p.endpoint.service.id = :serviceId " +
            "group by p.endpoint.id, p.endpoint.name, p.endpoint.service.name, p.endpoint.method " +
            "order by p.endpoint.id")
    List<ProbeSumAggregation> getSumEndpointAggregation(@Param("serviceId") Integer serviceId,
                                                        @Param("from") Long from);

    Optional<Service> getServiceByName(String name);

    Optional<Service> getServiceById(Integer id);
}

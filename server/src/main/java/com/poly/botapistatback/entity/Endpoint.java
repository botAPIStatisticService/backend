package com.poly.botapistatback.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "endpoint", uniqueConstraints = {
        @UniqueConstraint(name = "endpoint_method_uniq", columnNames = {"name", "service_id", "method"})
})
public class Endpoint {
    @Id
    @SequenceGenerator(name="jpaEndpointPkSeq", sequenceName="jpaEndpointPkSeq", allocationSize=1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "jpaEndpointPkSeq")
    private Integer id;

    @Column(name = "name")
    private String name;

    @ManyToOne(targetEntity = Service.class)
    private Service service;

    @Column(name = "method", nullable = false, length = 16)
    private String method;

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}

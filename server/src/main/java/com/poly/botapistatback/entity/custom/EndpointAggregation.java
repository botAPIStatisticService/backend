package com.poly.botapistatback.entity.custom;

public class EndpointAggregation {
    private Integer id;
    private String name;
    private Double averageTimeMs;
    private Double successRate;

    private String serviceName;

    private String method;

    public EndpointAggregation(Integer id,
                               String name,
                               Double averageTimeMs,
                               Double successRate,
                               String serviceName,
                               String method) {
        this.id = id;
        this.name = name;
        this.averageTimeMs = averageTimeMs;
        this.successRate = successRate;
        this.serviceName = serviceName;
        this.method = method;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getAverageTimeMs() {
        return averageTimeMs;
    }

    public Double getSuccessRate() {
        return successRate;
    }

    public String getMethod() {
        return method;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    @Override
    public String toString() {
        return "EndpointAggregation{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", averageTimeMs=" + averageTimeMs +
                ", successRate=" + successRate +
                ", serviceName='" + serviceName + '\'' +
                '}';
    }
}

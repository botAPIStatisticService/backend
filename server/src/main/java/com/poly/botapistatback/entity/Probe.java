package com.poly.botapistatback.entity;

import java.time.Instant;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.poly.botapistatback.entity.converter.SuccessConverter;

@Entity
@Table(name = "probe")
public class Probe {

    @Id
    @SequenceGenerator(name="jpaProbePkSeq", sequenceName="jpaProbePkSeq", allocationSize=1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "jpaProbePkSeq")
    private Long id;

    @ManyToOne(targetEntity = Endpoint.class)
    private Endpoint endpoint;

    @Column(name = "time")
    private Integer time;

    @Column(name = "success")
    @Convert(converter = SuccessConverter.class)
    private Boolean success;

    @Column(name = "probe_time")
    private Long probeTime = Instant.now().getEpochSecond();

    @Column(name = "code")
    private Integer code;

    public Long getId() {
        return id;
    }

    public Endpoint getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(Endpoint endpoint) {
        this.endpoint = endpoint;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Long getProbeTime() {
        return probeTime;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public void setProbeTime(Long probeTime) {
        this.probeTime = probeTime;
    }
}

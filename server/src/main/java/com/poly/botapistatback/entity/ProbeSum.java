package com.poly.botapistatback.entity;

public interface ProbeSum {
    Long getId();

    Endpoint getEndpoint();

    Long getSumTime();

    Long getSuccessCount();

    Long getCount();

    Long getProbeTruncTime();
}

package com.poly.botapistatback.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "probe_1d_sum")
public class ProbeDaySum implements ProbeSum{
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "sum_time", nullable = false)
    private Long sumTime;

    @Column(name = "success_count", nullable = false)
    private Long successCount;

    @Column(name = "count", nullable = false)
    private Long count;

    @Column(name = "probe_trunc_time", nullable = false, unique = true)
    private Long probeTruncTime;

    @ManyToOne
    private Endpoint endpoint;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public Long getSumTime() {
        return sumTime;
    }

    public Long getSuccessCount() {
        return successCount;
    }

    @Override
    public Long getCount() {
        return count;
    }

    @Override
    public Long getProbeTruncTime() {
        return probeTruncTime;
    }

    @Override
    public Endpoint getEndpoint() {
        return endpoint;
    }
}

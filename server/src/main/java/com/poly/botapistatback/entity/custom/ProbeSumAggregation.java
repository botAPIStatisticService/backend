package com.poly.botapistatback.entity.custom;

public class ProbeSumAggregation {
    private Integer id;
    private String name;
    private String method;
    private String serviceName;
    private Long sumTime;
    private Long sumSuccess;
    private Long sumCount;

    public ProbeSumAggregation(Integer id, String name, String method, String serviceName,
                               Long sumTime, Long sumSuccess, Long sumCount) {
        this.id = id;
        this.name = name;
        this.method = method;
        this.serviceName = serviceName;
        this.sumTime = sumTime;
        this.sumSuccess = sumSuccess;
        this.sumCount = sumCount;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getMethod() {
        return method;
    }

    public String getServiceName() {
        return serviceName;
    }

    public Long getSumTime() {
        return sumTime;
    }

    public Long getSumSuccess() {
        return sumSuccess;
    }

    public Long getSumCount() {
        return sumCount;
    }

    @Override
    public String toString() {
        return "ProbeSumAggregation{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", method='" + method + '\'' +
                ", serviceName='" + serviceName + '\'' +
                ", sumTime=" + sumTime +
                ", sumSuccess=" + sumSuccess +
                ", sumCount=" + sumCount +
                '}';
    }
}

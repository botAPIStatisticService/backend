package com.poly.botapistatback.dto.converter;

import com.poly.botapistatback.dto.ProbeDto;
import com.poly.botapistatback.entity.Probe;

public class ProbeConv {
    public static ProbeDto toDto(Probe entity) {
        ProbeDto result = new ProbeDto();
        result.setId(entity.getId());
        result.setSuccess(entity.getSuccess());
        result.setTime(entity.getTime());
        result.setEndpointId(entity.getEndpoint().getId());
        result.setProbeTime(entity.getProbeTime());
        result.setCode(entity.getCode());
        return result;
    }
}

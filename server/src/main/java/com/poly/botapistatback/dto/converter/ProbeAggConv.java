package com.poly.botapistatback.dto.converter;

import com.poly.botapistatback.dto.ProbeAggDto;
import com.poly.botapistatback.entity.ProbeSum;

public class ProbeAggConv {
    public static ProbeAggDto toDto(ProbeSum probeSum) {
        return new ProbeAggDto(probeSum.getSumTime(),
                probeSum.getSuccessCount(),
                probeSum.getCount(),
                probeSum.getProbeTruncTime());
    }
}

package com.poly.botapistatback.dto;

public class ProbeAggDto {
    private Long sumTime;
    private Long successCount;
    private Long count;
    private Long probeTruncTime;

    public ProbeAggDto(Long sumTime, Long successCount, Long count, Long probeTruncTime) {
        this.sumTime = sumTime;
        this.successCount = successCount;
        this.count = count;
        this.probeTruncTime = probeTruncTime;
    }

    public Long getSumTime() {
        return sumTime;
    }

    public Long getSuccessCount() {
        return successCount;
    }

    public Long getCount() {
        return count;
    }

    public Long getProbeTruncTime() {
        return probeTruncTime;
    }
}

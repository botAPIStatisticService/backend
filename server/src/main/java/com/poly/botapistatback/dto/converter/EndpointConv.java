package com.poly.botapistatback.dto.converter;

import com.poly.botapistatback.dto.EndpointDto;
import com.poly.botapistatback.entity.Endpoint;

public class EndpointConv {
    public static EndpointDto toDto(Endpoint entity) {
        EndpointDto result = new EndpointDto();
        result.setId(entity.getId());
        result.setName(entity.getName());
        result.setMethod(entity.getMethod());
        return result;
    }
}

package com.poly.botapistatback.dto.converter;

import com.poly.botapistatback.dto.ServiceDto;
import com.poly.botapistatback.entity.Service;

public class ServiceConv {
    public static ServiceDto toDto(Service entity) {
        ServiceDto dto = new ServiceDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        return dto;
    }
}

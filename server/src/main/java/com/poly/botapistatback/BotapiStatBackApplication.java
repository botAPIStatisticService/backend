package com.poly.botapistatback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BotapiStatBackApplication {

    public static void main(String[] args) {
        SpringApplication.run(BotapiStatBackApplication.class, args);
    }

}

package com.poly.botapistatback.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.poly.botapistatback.dto.EndpointDto;
import com.poly.botapistatback.dto.ProbeDto;
import com.poly.botapistatback.payload.response.EndpointStatistic;
import com.poly.botapistatback.payload.response.Event;
import com.poly.botapistatback.service.EndpointService;

@RestController
@RequestMapping("/endpoint")
public class EndpointController {

    @Autowired
    private EndpointService endpointService;

    @GetMapping(value = "/{endpointId}")
    public EndpointStatistic getById(@PathVariable(value = "endpointId") Integer endpointId,
                                   @RequestParam("offset") Integer offset,
                                   @RequestParam("length") Integer length) {
        EndpointStatistic endpointStatistic = new EndpointStatistic();
        EndpointDto endpoint = endpointService.findById(endpointId);
        endpointStatistic.setId(endpoint.getId());
        endpointStatistic.setName(endpoint.getName());
        endpointStatistic.setMethod(endpoint.getMethod());
        List<ProbeDto> probes =  endpointService.getProbe(endpoint, offset, length);
        List<Event> events = probes.stream()
                        .map(probeDto -> {
                            Event event = new Event();
                            event.setId(probeDto.getId());
                            event.setSuccess(probeDto.getSuccess());
                            event.setResponseMS(probeDto.getTime());
                            event.setRequestTime(probeDto.getProbeTime());
                            event.setResponseCode(probeDto.getCode());
                            return event;
                        }).collect(Collectors.toList());
        endpointStatistic.setEvents(events);
        return endpointStatistic;
    }
}

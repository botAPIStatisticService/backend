package com.poly.botapistatback.controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.poly.botapistatback.dto.EndpointDto;
import com.poly.botapistatback.dto.ProbeAggDto;
import com.poly.botapistatback.dto.ProbeDto;
import com.poly.botapistatback.dto.ServiceDto;
import com.poly.botapistatback.entity.custom.EndpointAggregation;
import com.poly.botapistatback.entity.custom.ProbeSumAggregation;
import com.poly.botapistatback.payload.response.AvgEvent;
import com.poly.botapistatback.payload.response.EndpointAvgStat;
import com.poly.botapistatback.payload.response.EndpointSummary;
import com.poly.botapistatback.service.EndpointService;
import com.poly.botapistatback.service.ProbeSumService;
import com.poly.botapistatback.service.ServiceService;

@RestController
@RequestMapping("/endpoints")
public class ServiceController {

    @Autowired
    ServiceService service;

    @Autowired
    EndpointService endpointService;

    @Autowired
    ProbeSumService probeSumService;

    @GetMapping("/{serviceName}/summary")
    public List<EndpointSummary> summary(@PathVariable("serviceName") String serviceName) {

        List<ProbeSumAggregation> endpointAggregations;
        LocalDateTime today = LocalDate.now(ZoneOffset.UTC).atStartOfDay();
        today = today.minusDays(30);
        long from = today.toEpochSecond(ZoneOffset.UTC);
        List<EndpointDto> endpoints;
        if (serviceName.equals("all")) {
            endpointAggregations = service.getAllProbeSumAggregation(from);
            endpoints = endpointService.getAll();
        } else {
            ServiceDto serviceDto = service.getServiceByName(serviceName);
            endpointAggregations = service.getProbeSumAggregationByService(serviceDto.getId(), from);
            endpoints = endpointService.getByService(serviceDto);
        }
        List<EndpointSummary> result = endpointAggregations.stream().map(probeSum -> {
            EndpointSummary endpointSummary = new EndpointSummary();
            endpointSummary.setId(probeSum.getId());
            endpointSummary.setName(probeSum.getName());
            endpointSummary.setSuccessRate(probeSum.getSumCount() == 0 ?
                    0f : Math.round(((float) probeSum.getSumSuccess() / probeSum.getSumCount() * 100) * 100) / 100.0f);
            endpointSummary.setAverageResponseMS(probeSum.getSumSuccess() == 0 ?
                    0 : (int) (probeSum.getSumTime() / probeSum.getSumSuccess()));
            endpointSummary.setMethod(probeSum.getMethod());
            return endpointSummary;
        }).collect(Collectors.toList());
        Set<Integer> endpointsIds = endpoints.stream().map(EndpointDto::getId).collect(Collectors.toSet());
        Set<Integer> resultIds = result.stream().map(EndpointSummary::getId).collect(Collectors.toSet());
        endpointsIds.removeAll(resultIds);
        endpoints.stream().filter(k -> endpointsIds.contains(k.getId()))
                .forEach(k -> {
                    EndpointSummary summary = new EndpointSummary();
                    summary.setId(k.getId());
                    summary.setName(k.getName());
                    summary.setMethod(k.getMethod());
                    summary.setSuccessRate(0.0f);
                    summary.setAverageResponseMS(0);
                    result.add(summary);
                });
        return result;
    }

    @GetMapping(value = "/{serviceName}")
    public List<EndpointAvgStat> getAvgStats(@PathVariable("serviceName") String serviceName,
                                             @RequestParam("frequency") Integer frequency,
                                             @RequestParam("period") Integer period) {
        LocalDateTime today = LocalDate.now(ZoneOffset.UTC).atStartOfDay();
        LocalDateTime start = today.minusDays(period);
        ServiceDto serviceDto;
        List<EndpointDto> endpoints;
        if (serviceName.equals("all")) {
            endpoints = endpointService.getAll();
        } else {
            serviceDto = service.getServiceByName(serviceName);
            endpoints = endpointService.getByService(serviceDto);
        }
        long startSeconds = start.toEpochSecond(ZoneOffset.UTC);
        List<EndpointAvgStat> result = new ArrayList<>();
        for (EndpointDto endpoint : endpoints) {
            List<ProbeAggDto> probeAggList = probeSumService.getByEndpointFrom(endpoint.getId(),
                    startSeconds, frequency);
            List<AvgEvent> endpointEvents = probeAggList.stream()
                    .map((e) -> new AvgEvent(
                            e.getCount() == 0 ? 0 : Math.round(((float) e.getSuccessCount()) / e.getCount() * 100) / 100.0f,
                            e.getSuccessCount() == 0 ? 0 : ((float) e.getSumTime()) / e.getSuccessCount(),
                            e.getProbeTruncTime())
                    ).collect(Collectors.toList());
            result.add(new EndpointAvgStat(
                    endpoint.getId(),
                    endpoint.getName(),
                    endpoint.getMethod(),
                    endpointEvents
            ));
        }
        return result;
    }
}
